﻿// PracticalWork_6.5.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <list>
#include <algorithm>

using namespace std;

int main()
{
    // generate — для заполнения списка из 10 элементов целочисленными значениями
    list<int> RandomList;
    RandomList.resize(10);
    srand(10);
    generate(RandomList.begin(), RandomList.end(), []()->int { return rand(); });
    cout << "generate" << endl;
    for (auto i : RandomList) { cout << i << endl; }
    cout << endl;

    // transform — для целочисленного списка из 10 элементов
    cout << "transform" << endl;
    transform(RandomList.begin(), RandomList.end(), RandomList.begin(), [](const int& OldValue) { return OldValue%10; });
    for (auto i : RandomList) { cout << i << endl; }
    cout << endl;

    // remove — для фильтрации чисел больше 3 в целочисленном списке из 10 элементов
    cout << "remove" << endl;
    RandomList.remove_if( [](const int& CurrentValue) { return CurrentValue > 3; });
    for (auto i : RandomList) { cout << i << endl; }
}